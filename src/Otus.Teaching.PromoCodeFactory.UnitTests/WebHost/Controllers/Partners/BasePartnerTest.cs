﻿using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class BasePartnerTest : IDisposable
    {
        private readonly Mock<IRepository<Partner>> _IPartnerRepositoryMock;
        private readonly PartnersController _partnerController;
        private readonly Partner _partnerFilled;
        private readonly PartnerPromoCodeLimit _partnerPromoCodelimitFilled;
        private readonly Fixture _fixture;
        private readonly SetPartnerPromoCodeLimitRequest _setPartnerPromoCodeLimitRequestFilled;

        public Mock<IRepository<Partner>> PartnerRepositoryMock => _IPartnerRepositoryMock;
        public PartnersController PartnerController => _partnerController;
        public Partner PartnerFilled => _partnerFilled;
        public PartnerPromoCodeLimit PartnerPromoCodelimitFilled => _partnerPromoCodelimitFilled;
        public SetPartnerPromoCodeLimitRequest SetPartnerPromoCodeLimitRequestFilled => _setPartnerPromoCodeLimitRequestFilled;

        public BasePartnerTest()
        {
            _fixture = new Fixture();
            _IPartnerRepositoryMock = new Mock<IRepository<Partner>>();
            _partnerController = new PartnersController(_IPartnerRepositoryMock.Object);
            _setPartnerPromoCodeLimitRequestFilled = _fixture.Create<SetPartnerPromoCodeLimitRequest>();
            _partnerFilled = _fixture.Build<Partner>().With(x=>x.IsActive, true).With(x=>x.PartnerLimits, new List<PartnerPromoCodeLimit>()).Create();
            _partnerPromoCodelimitFilled = _fixture.Build<PartnerPromoCodeLimit>().With(x=>x.Partner, _partnerFilled).Create();
            _partnerFilled.PartnerLimits.Add(_partnerPromoCodelimitFilled);
        }

        public void Dispose()
        {
        }
    }
}
