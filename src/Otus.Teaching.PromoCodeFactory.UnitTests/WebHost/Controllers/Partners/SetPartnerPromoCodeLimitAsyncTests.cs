﻿using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using FluentAssertions;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : BasePartnerTest
    {
        private readonly Fixture _fixture;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture();
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404.
        /// </summary>
        [Fact]
        public async void Partner_notFound_Return_404()
        {
            //arrange
            Guid id = Guid.Empty;

            //act
            var result = await PartnerController.SetPartnerPromoCodeLimitAsync(id, It.IsAny<SetPartnerPromoCodeLimitRequest>());

            //assert            
            result.Should().BeOfType<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то нужно выдать ошибку 400.
        /// </summary>
        [Fact]
        public async void Partner_isNotActive_Return_400()
        {
            //arrange            
            PartnerFilled.IsActive = false;
            PartnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(PartnerFilled);

            //act
            var result = await PartnerController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), It.IsAny<SetPartnerPromoCodeLimitRequest>());

            //assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит не закончился.
        /// </summary>
        [Fact]
        public async void PartnerSetLimit_ifLimitHasNotEnded_resetNumberIssuedOfPromocodes()
        {
            //arrange
            int expectedNumber = 0;
            int limitInitValue = 5;
            PartnerFilled.NumberIssuedPromoCodes = limitInitValue;
            PartnerPromoCodelimitFilled.CancelDate = null;
            PartnerRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(PartnerFilled);

            // Act
            await PartnerController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), SetPartnerPromoCodeLimitRequestFilled);

            // Assert
            PartnerFilled.NumberIssuedPromoCodes.Should().Be(expectedNumber);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы не должны обнулять количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился.
        /// </summary>
        [Fact]
        public async void PartnerSetLimit_ifLimitHasEnded_NumberIssuedOfPromocodesIsNotReset()
        {
            //arrange
            int expectedNumber = 0;
            PartnerFilled.NumberIssuedPromoCodes = expectedNumber;

            PartnerRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(PartnerFilled);

            // Act
            await PartnerController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), SetPartnerPromoCodeLimitRequestFilled);

            // Assert            
            PartnerFilled.NumberIssuedPromoCodes.Should().Be(expectedNumber);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит.
        /// </summary>
        [Fact]
        public async void PartnerSetLimit_ifLimitHasNotEnded_disablePreviousLimit()
        {
            //arrange                        
            PartnerPromoCodelimitFilled.CancelDate = null;

            PartnerRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(PartnerFilled);

            // Act
            await PartnerController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), SetPartnerPromoCodeLimitRequestFilled);

            // Assert
            PartnerPromoCodelimitFilled.CancelDate.Should().NotBeNull();
        }

        /// <summary>
        /// Лимит должен быть больше 0.
        /// </summary>
        [Fact]
        public async void InRequest_limitLessThanZero_Return_400()
        {
            //arrange
            int limitLessThanZero = -1;

            SetPartnerPromoCodeLimitRequestFilled.Limit = limitLessThanZero;
            PartnerRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(PartnerFilled);

            //act
            var result = await PartnerController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), SetPartnerPromoCodeLimitRequestFilled);

            //assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        /// <summary>
        /// Нужно убедиться, что сохранили новый лимит в базу данных.
        /// </summary>
        /// <returns></returns>
        [Theory]
        [InlineData(1)]
        [InlineData(12)]
        [InlineData(123)]
        public async void SetPartnerPromoCodeLimitAsync_ExistingPartner_ReturnsCreatedAtAction(int expectedLimit)
        {
            // Arrange
            Guid expectedPartnerId = Guid.NewGuid();
            SetPartnerPromoCodeLimitRequestFilled.Limit = expectedLimit;
            PartnerFilled.Id = expectedPartnerId;

            PartnerRepositoryMock.Setup(r => r.GetByIdAsync(expectedPartnerId))
            .ReturnsAsync(PartnerFilled);

            var expectedActionName = nameof(PartnerController.GetPartnerLimitAsync);

            // Act
            IActionResult result = await PartnerController.SetPartnerPromoCodeLimitAsync(expectedPartnerId, SetPartnerPromoCodeLimitRequestFilled);
            var actualLimit = PartnerFilled.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue).Limit;

            // Assert
            PartnerRepositoryMock.Verify(x => x.UpdateAsync(PartnerFilled), Times.Once);
            result.Should().BeOfType<CreatedAtActionResult>();
            CreatedAtActionResult createdAtAction = (CreatedAtActionResult)result;
            createdAtAction.ActionName.Should().Be(expectedActionName);
            createdAtAction.RouteValues["id"].Should().Be(expectedPartnerId);
            actualLimit.Should().Be(expectedLimit);
        }
    }
}
